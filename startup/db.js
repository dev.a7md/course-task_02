const mongoose = require('mongoose');
const debug = require('debug')('app:db');
const config = require('config');

const opts = {
  useNewUrlParser: true,
  useCreateIndex: true
};

module.exports = () => {
  mongoose.connect(config.get('db.uri'), opts)
    .then(() => debug('Connected...'))
    .catch((err) => debug(err.message));
};
