const usersRouter = require('../routes/usersRouter');

module.exports = (app) => {
  app.use('/api/users', usersRouter);
};
