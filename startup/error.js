const createError = require('http-errors');

module.exports = (app) => {
  app.use(function (req, res, next) {
    next(createError(404));
  });

  app.use(function (err, req, res, next) {
    res.send(err.message);
  });
};
