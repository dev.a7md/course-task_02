const Joi = require('joi');

module.exports = (req, res, next) => {
  const joiSchema = {
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(512).required()
  };

  const {error} = Joi.validate(req.body, joiSchema);
  if (error) return next(error);

  next();
};
