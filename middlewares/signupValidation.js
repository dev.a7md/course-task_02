const Joi = require('joi');

module.exports = (req, res, next) => {
  const joiSchema = {
    name: Joi.string().min(4).max(50).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(512).required(),
    role: Joi.string().required()
  };

  const {error} = Joi.validate(req.body, joiSchema);
  if (error) return next(error);

  next();
};
