const express = require('express');
const router = express.Router();

const User = require('../models/userModel');

const signValidation = require('../middlewares/signupValidation');
const loginValidation = require('../middlewares/loginValidation');
const auth = require('../middlewares/auth');
const isAdmin = require('../middlewares/admin');

router.get('/', auth, async function (req, res) {
  const users = await User.find();
  res.send(users);
});

router.post('/signup', signValidation, async (req, res) => {
  const {name, email, password, role} = req.body;
  const user = new User({
    name,
    email,
    password,
    role
  });
  await user.save();
  res.header('x-auth-token', user.generateToken());
  res.send('done');
});

router.post('/signin', loginValidation, async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});
  if (!user) return res.status(400).send('Invalid email or password');

  if (!user.comparePassword(password)) return res.status(400).send('Invalid email or password');

  res.header('x-auth-token', user.generateToken());
  res.send('Logged in');
});

router.delete('/:id', [auth, isAdmin], async (req, res) => {
  const user = await User.findById(req.params.id);
  if (!user) return res.status(404).send('User not found');

  await user.remove();
  res.send('Deleted');
});

module.exports = router;
