const express = require('express');
require('express-async-errors');

const app = express();
module.exports = app;

require('./startup/db')();
require('./startup/middleware')(app);
require('./startup/route')(app);
require('./startup/error')(app);
