const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('config');

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
    minLength: 4,
    maxLength: 50
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    min: 6,
    max: 1024,
    required: true
  },
  role: {
    type: String,
    required: true,
    enum: ['admin', 'user']
  }
});

userSchema.methods.generateToken = function () {
  const {email, role} = this;
  const opts = {
    email,
    role
  };
  return jwt.sign(opts, config.get('token'))
};

userSchema.methods.comparePassword = function (pass) {
  return bcrypt.compare(pass, this.password);
};

userSchema.pre('save', async function () {
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

module.exports = mongoose.model('user', userSchema);
